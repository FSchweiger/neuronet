package de.frizzware.neuronet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import de.frizzware.neuronet.neuro.Network;


public class MainActivity extends Activity implements View.OnClickListener{

    private static final int IMAGE_CAPTURE = 10;
    private static final int IMAGE_SIZE = 150;
    private static final int ANSWER_POSSIBILITIES = 10;

    private Network neuroNet;
    private Bitmap picture;
    private TextToSpeech tts;

    private ArrayList<String> items = new ArrayList<String>(ANSWER_POSSIBILITIES);

    private Button buttonLearn, buttonThink, buttonPhoto;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLearn = (Button) findViewById(R.id.buttonLearn);
        buttonThink = (Button) findViewById(R.id.buttonThink);
        buttonPhoto = (Button) findViewById(R.id.buttonPhoto);

        buttonLearn.setOnClickListener(this);
        buttonThink.setOnClickListener(this);
        buttonPhoto.setOnClickListener(this);

        imageView = (ImageView) findViewById(R.id.imageView);

        neuroNet = new Network(IMAGE_SIZE * IMAGE_SIZE, ANSWER_POSSIBILITIES);
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tts.shutdown();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            picture = Bitmap.createScaledBitmap(imageBitmap, IMAGE_SIZE, IMAGE_SIZE, false);
            imageView.setImageBitmap(picture);
        }
    }

    private int[] bitmapToArray(Bitmap input){
        int[] result = new int[IMAGE_SIZE * IMAGE_SIZE];
        int index = 0;

        for(int x = 0; x < IMAGE_SIZE; x++){
            for(int y = 0; y < IMAGE_SIZE; y++){
                int value = 0, pixel = input.getPixel(x, y);
                if((Color.red(pixel) < 80) && (Color.green(pixel) < 80) && (Color.blue(pixel) < 80)){
                    value = 1;
                }
                result[index] = value;
                index++;
            }
        }

        return result;
    }

    private void teach(Bitmap image, int answer){
        neuroNet.learn(bitmapToArray(image), answer);
    }

    private int think(Bitmap image){
        return neuroNet.think(bitmapToArray(image));
    }

    @Override
    public void onClick(View view) {
        if(view.equals(buttonPhoto)){
            dispatchTakePictureIntent();
        }else if(view.equals(buttonLearn)){
            showNewDialog();
        }else{
            int answer = think(picture);
            speak("Ich meine es ist " + items.get(answer));
            Toast.makeText(getApplicationContext(), "Ergebnis: " + items.get(answer), Toast.LENGTH_SHORT).show();
        }
    }

    private void speak(String what){
        tts.speak(what, TextToSpeech.QUEUE_FLUSH, null);
    }

    private void showNewDialog(){
        final EditText t = new EditText(this);
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Was ist das?").setView(t)
                .setItems(items.toArray(new String[0]), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        teach(picture, i);
                        Toast.makeText(getApplicationContext(), "Objekt erfolgreich optimiert.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        items.add(t.getText().toString());
                        teach(picture, items.size() - 1);
                        Toast.makeText(getApplicationContext(), "Objekt erfolgreich gelernt!", Toast.LENGTH_SHORT).show();
                    }
                }).create().show();
    }
}
