package de.frizzware.neuronet.neuro;

/**
 * Created by Frederik on 22.07.2014.
 */
public class Link {

    public Link(){
        weight = 0;
    }

    public Neuron neuron;
    public double weight;

}
