package de.frizzware.neuronet.neuro;

import java.util.ArrayList;

/**
 * Created by Frederik on 22.07.2014.
 */
public class Input {

    public Input(int connSize){
        links = new ArrayList<Link>(connSize);
    }

    public ArrayList<Link> links;

}
