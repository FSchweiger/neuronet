package de.frizzware.neuronet.neuro;

import java.util.ArrayList;

/**
 * Created by Frederik on 22.07.2014.
 */
public class Neuron {

    public Neuron(int linkSize){
        links = new ArrayList<Link>(linkSize);
        power = 0;
    }

    public ArrayList<Link> links;
    public double power;

}
