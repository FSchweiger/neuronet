package de.frizzware.neuronet.neuro;

import java.util.ArrayList;

/**
 * Created by Frederik on 22.07.2014.
 */
public class Network {

    ArrayList<Input> inputs;
    ArrayList<Neuron> neurons;

    public Network(int inputSize, int outputSize){
        inputs = new ArrayList<Input>(inputSize);
        neurons = new ArrayList<Neuron>(outputSize);

        for(int n = 0; n < outputSize; n++){
            neurons.add(new Neuron(inputSize));
        }

        for(int x = 0; x < inputSize; x++){
            Input i = new Input(outputSize);

            for(int y = 0; y < outputSize; y++){
                Link conn = new Link();
                conn.neuron = neurons.get(y);
                i.links.add(conn);
                neurons.get(y).links.add(conn);
            }

            inputs.add(i);
        }
    }

    public int think(int[] input){

        for(int x = 0; x < inputs.size(); x++){
            Input i = inputs.get(x);

            for(Link l : i.links){
                l.neuron.power += l.weight * input[x];
            }
        }

        int resultIndex = 0;
        for(int y = 0; y < neurons.size(); y++){
            if(neurons.get(resultIndex).power < neurons.get(y).power){
                resultIndex = y;
            }
        }

        for(Neuron n : neurons){
            n.power = 0;
        }

        return resultIndex;
    }

    public void learn(int[] input, int answerIndex){
        Neuron rightNeuron = neurons.get(answerIndex);

        for(int x = 0; x < rightNeuron.links.size(); x++){
            Link l = rightNeuron.links.get(x);
            l.weight = l.weight + 0.5 * (input[x] - l.weight);
        }
    }

}
